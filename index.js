const buttons = Array.from(document.getElementsByClassName('button'));
const result = document.getElementById('result');

const darkModeToggle = document.getElementById('dark-mode-toggle');
const calculator = document.querySelector('.calculator');

darkModeToggle.addEventListener('change', () => {
  if (darkModeToggle.checked) {
    calculator.classList.add('dark-mode');
  } else {
    calculator.classList.remove('dark-mode');
  }
});


buttons.forEach(button => {
  button.addEventListener('click', () => {
    const buttonText = button.innerText;
    if (buttonText === 'Clear') {
      result.value = '';
    } else if (buttonText === '=') {
      try {
        result.value = eval(result.value);
      } catch (error) {
        result.value = 'Error';
      }
    } else {
      result.value += buttonText;
    }
  });
});

// Find the division button
const divisionButton = document.querySelector('.button[data-operation="/"]');

// Add an event listener to the division button
divisionButton.addEventListener('click', () => {
  result.value += '/';
});

